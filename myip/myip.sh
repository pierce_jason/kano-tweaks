#!/bin/sh

# Version 1.0

# Depends on package sysvbanner

# Find the active interface
iface=`/sbin/route -n | grep 'UG' | rev | cut -d ' ' -f 1 | rev`;
echo Interface to gateway is $iface;

# Find the IP of the active interface
myip=`/sbin/ifconfig $iface | grep 'inet addr' | cut -d ':' -f 2| cut -d ' ' -f1`;

myipA=`echo $myip | cut -d '.' -f 1`;
myipA=`printf "%3s" $myipA | sed "s/ /0/g"`;

myipB=`echo $myip | cut -d '.' -f 2`;
myipB=`printf "%3s" $myipB | sed "s/ /0/g"`;

myipC=`echo $myip | cut -d '.' -f 3`;
myipC=`printf "%3s" $myipC | sed "s/ /0/g"`;

myipD=`echo $myip | cut -d '.' -f 4`;
myipD=`printf "%3s" $myipD | sed "s/ /0/g"`;


# Now show it to us!
banner "$myipA.$myipB";
banner "$myipC.$myipD";

echo "Press any key to continue ...";
read nothing;
